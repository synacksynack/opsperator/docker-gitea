#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
grep -v ^gitea /etc/passwd >/tmp/passwd
echo "gitea:x:$(id -u):0:gitea user:/data:/sbin/nologin" >>/tmp/passwd
cat /tmp/passwd >/etc/passwd
rm -f /tmp/passwd

export GITEA_WORK_DIR=/home/gitea
export USER=gitea
export USERNAME=gitea
export HOME=/home/gitea

if ls /usr/local/share/ca-certificates/ 2>/dev/null | grep -E '(crt|pem)' >/dev/null; then
    echo ">>> Updating Trusted CA Certificates"
    update-ca-certificates
fi

if ! test -s /home/gitea/conf/app.ini; then
    mkdir -p /data/uploads/avatars/people /data/uploads/avatars/repo \
	/data/sqlite /data/git/repositories /data/uploads/attachments
    cat <<EOF >/home/gitea/conf/app.ini
APP_NAME = Gitea: Git with a cup of tea
RUN_USER = gitea
RUN_MODE = prod

[repository]
ROOT = /data/git/repositories

[server]
DOMAIN           = git.demo.local
SSH_DOMAIN       = git.demo.local
HTTP_PORT        = 3000
ROOT_URL         = https://git.demo.local
DISABLE_SSH      = false
START_SSH_SERVER = true
SSH_PORT         = 2022
SSH_LISTEN_PORT  = 2022
LFS_START_SERVER = false

[database]
PATH    = /data/sqlite/gitea.db
DB_TYPE = sqlite3

[session]
PROVIDER = file

[picture]
DISABLE_GRAVATAR = false
AVATAR_UPLOAD_PATH = /data/uploads/avatars/people
REPOSITORY_AVATAR_UPLOAD_PATH = /data/uploads/avatars/repo

[attachment]
PATH = /data/uploads/attachments

[log]
MODE = console
LEVEL = info
ROUTER = console
ROOT_PATH = /data/gitea/log

[security]
INSTALL_LOCK = true
SECRET_KEY   = abcsecretkey

[service]
DISABLE_REGISTRATION = false
REQUIRE_SIGNIN_VIEW  = false
EOF
fi

exec /home/gitea/gitea --config=/home/gitea/conf/app.ini web
